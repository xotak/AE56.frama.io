// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'My Site',
  tagline: 'REEEEEEEEEEEEEEEEEE',
  url: 'https://www.sophro-naturo-imp-guegon.fr',
  baseUrl: '/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: false,
        blog: {
          showReadingTime: true,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'My Site',
        logo: {
          alt: 'My Site Logo',
          src: 'img/logo.svg',
        },
        items: [
          {to: '/IMP', label: 'Réflèxe arcahïques', position: 'left'},
          {to: '/sophrologie', label: 'Sophrologie', position: 'left'},
          {to: '/naturopathie', label: 'Naturopathie', position: 'left'},
          {to: '/massages', label: 'Massages mamans et bébés', position: 'left'},
          {to: '/reflexo', label: 'Réflexologie', position: 'left'},
          {to: '/blog', label: 'Blog', position: 'left'},
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Contenus',
            items: [
              {
                label: 'Sophrologie',
                to: '/sophrologie',
              },
              {
                label: 'Naturopathie',
                to: '/naturopathie',
              },
            ],
          },
        ],
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),

    plugins: [
      [
        '@docusaurus/plugin-content-blog',
        {
          /**
           * Required for any multi-instance plugin
           */
          id: 'sophro',
          /**
           * URL route for the blog section of your site.
           * *DO NOT* include a trailing slash.
           */
          routeBasePath: 'sophrologie',
          /**
           * Path to data on filesystem relative to site dir.
           */
          path: './sophrologie',
          blogSidebarCount: 0
        },
      ],
      [
        '@docusaurus/plugin-content-blog',
        {
          /**
           * Required for any multi-instance plugin
           */
          id: 'naturo',
          /**
           * URL route for the blog section of your site.
           * *DO NOT* include a trailing slash.
           */
          routeBasePath: 'naturopathie',
          /**
           * Path to data on filesystem relative to site dir.
           */
          path: './naturopathie',
          blogSidebarCount: 0
        },
      ],
      [
        '@docusaurus/plugin-content-blog',
        {
          /**
           * Required for any multi-instance plugin
           */
          id: 'arcahiques',
          /**
           * URL route for the blog section of your site.
           * *DO NOT* include a trailing slash.
           */
          routeBasePath: 'IMP',
          /**
           * Path to data on filesystem relative to site dir.
           */
          path: './IMP',
          blogSidebarCount: 0
        },
      ],
      [
        '@docusaurus/plugin-content-blog',
        {
          /**
           * Required for any multi-instance plugin
           */
          id: 'massages',
          /**
           * URL route for the blog section of your site.
           * *DO NOT* include a trailing slash.
           */
          routeBasePath: 'massages',
          /**
           * Path to data on filesystem relative to site dir.
           */
          path: './massages',
          blogSidebarCount: 0
        },
      ],
      [
        '@docusaurus/plugin-content-blog',
        {
          /**
           * Required for any multi-instance plugin
           */
          id: 'relfexo',
          /**
           * URL route for the blog section of your site.
           * *DO NOT* include a trailing slash.
           */
          routeBasePath: 'reflexo',
          /**
           * Path to data on filesystem relative to site dir.
           */
          path: './reflexo',
          blogSidebarCount: 0
        },
      ],
    ],
    
};

module.exports = config;
